package datastructure;
import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows;
    private int columns;
    private CellState[][] gridCelle;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
		this.columns = columns;
		this.gridCelle = new CellState[rows][columns];
		for (int r=0; r<rows; r++) {
			for(int c=0; c<columns; c++) {
				gridCelle[r][c] = initialState;
			}
		}
	}


    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(row<0 || row>=numRows() || column<0 || column>=numColumns())
            throw new IndexOutOfBoundsException();

        gridCelle[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row<0 || row>=numRows() || column<0 || column>=numColumns())
            throw new IndexOutOfBoundsException();
        return gridCelle[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid gridKopi = new CellGrid(rows, columns, CellState.DEAD);
        for(int r=0; r<rows; r++) {
        	for(int c=0; c<columns; c++) {
        		gridKopi.set(r, c, this.get(r, c));
        	}
        }
        return gridKopi;
    }
    
}
